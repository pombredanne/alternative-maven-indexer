netbeans-alternative-mavenindexer
=================================

Experimental netbeans module using a maven-indexer with a newer Lucene version.

Implications:
- Smaller index/field store (about 50%)
- Does not work with old indices

See the following issues for more information:

- https://netbeans.org/bugzilla/show_bug.cgi?id=239704
- http://jira.codehaus.org/browse/MINDEXER-77

If you run your own (development) builk you may need to install the yenta API module manually
as well. You can download the yenta NBM here: 
https://oss.sonatype.org/content/groups/public/org/netbeans/contrib/yenta/api/1.1/