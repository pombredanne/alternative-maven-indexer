package de.everflux.netbeans.maven.indexer.module;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.contrib.yenta.Yenta;
import org.netbeans.modules.maven.indexer.spi.RepositoryIndexerImplementation;
import org.openide.util.Lookup;

public class Installer extends Yenta {

    @Override
    protected Set<String> friends() {
        String[] s = new String[] {
            "org.netbeans.modules.maven.embedder"
        };
        return new HashSet<String>(Arrays.asList(s));
    }

    @Override
    protected Set<String> siblings() {
        String[] s = new String[] {
            "org.netbeans.modules.maven.indexer"
        };
        return new HashSet<String>(Arrays.asList(s));
    }
    

    @Override
    public void restored() {
        super.restored(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    @SuppressWarnings("deprecation")
    public @Override void close() {
        Logger LOG = Logger.getLogger(de.everflux.netbeans.maven.indexer.module.Installer.class.getName());
        if (!Cancellation.cancelAll()) {
            // Cf. #188883. Hard to kill HTTP connections.
            for (Thread t : RemoteIndexTransferListener.getActiveTransfersOrScans()) {
                LOG.log(Level.WARNING, "Killing Maven Repo Transfer thread {0} on system exit...", t.getName());
                t.interrupt();
                try {
                    t.join(1000);
                } catch (InterruptedException x) {
                    LOG.log(Level.INFO, null, x);
                }
                if (t.isAlive()) {
                    LOG.warning("...hard stop required.");
                    t.stop(new Cancellation());
                }
            }
        }
        ((NexusRepositoryIndexerImpl)Lookup.getDefault().lookup(RepositoryIndexerImplementation.class)).shutdownAll();
    }


}
